int detailLevel = 1;

JSONObject allChannels;

ArrayList<Time> seasonTimes;
ArrayList<Time> yearTimes;

String[] names;

StringDict nameLookup;
HashMap<String,int[]> colorLookup;

color full;
color light;
color lightest;

void setup() {
  //CANVAS SIZE, COLORS
  
  size(840, 728);
  
  if(detailLevel >= 3) {
    full = color(255);
    light = color(100);
    lightest = color(65);
  } else {
    full = color(0);
    light = color(160);
    lightest = color(200);
  }
  
  background(255-full);
  
  
  //LOAD EXTERNAL DATA
  
  allChannels = loadJSONObject("data/multipleChannels.json");
  
  seasonTimes = new ArrayList<Time>();
  seasonTimes.add(new Time(2012, 4, 13));
  seasonTimes.add(new Time(2013, 6, 10));
  seasonTimes.add(new Time(2014, 10, 18));
  seasonTimes.add(new Time(2016, 2, 25));
  seasonTimes.add(new Time(2017, 4, 13));
  seasonTimes.add(new Time(2018, 7, 19));
  seasonTimes.add(new Time(2020, 2, 28));
  seasonTimes.add(new Time(2021, 6, 19));
  seasonTimes.add(new Time(2021, 12, 22));
  seasonTimes.add(new Time(2022, 3, 5));
  
  yearTimes = new ArrayList<Time>();
  for(int i = 2012; i <= 2023; i++) {
    yearTimes.add(new Time(i, 1, 1));
  }
  
  names = loadStrings("data/order.txt");
  
  String[] meta = loadStrings("data/meta.csv");
  
  nameLookup = new StringDict();
  colorLookup = new HashMap<String,int[]>();
  
  for(int i = 0; i < meta.length; i++) {
    String[] row = split(meta[i],',');
    nameLookup.set(row[0],row[1]);
    
    int[] col = new int[3];
    for(int j = 0; j < 3; j++) {
      col[j] = int(row[j+2]);
    }
    colorLookup.put(row[0],col);
  }
  
  
  //FOR EACH CHANNEL
  
  IntDict totalVideos = new IntDict();
  FloatDict activeTime = new FloatDict();
  FloatDict joinDate = new FloatDict();
  
  Time start = seasonTimes.get(0);
  Time end = new Time(year(), month(), day(), hour(), minute(), second());
  Time totalDuration = subtract(end, start);
  
  for(int n = 0; n < names.length; n++) {
    JSONArray channel = allChannels.getJSONArray(names[n]);
    
    //GET VIDEO TIMES
    
    ArrayList<Time> times = new ArrayList<Time>();
    
    for(int i = channel.size()-1; i >= 0; i--) {
      JSONObject video = channel.getJSONObject(i);
      JSONObject snippet = video.getJSONObject("snippet");
      
      String title = snippet.getString("title");
      Time publishedAt = isoTimestamp(snippet.getString("publishedAt"));
      
      if(title.toLowerCase().contains("hermitcraft")) times.add(publishedAt); //filter
    }
    
    totalVideos.set(names[n], times.size());
    joinDate.set(names[n], (float) (times.get(0).value()-start.value()));
    
    //DISPLAY ACTIVITY BAR
    
    double totalTime = 0;
    
    for(int i = 0; i < times.size()-1; i++) {
      Time a = times.get(i);
      Time b = times.get(i+1);
      Time duration = subtract(b, a);
      
      if(!greaterThan(duration, duration(0, 2, 14))) totalTime += duration.value();
      
      noStroke();
      fill(activityColor(duration,names[n]));
      
      float h = (float) (100.0/(duration.value()/(1000*60*60*24)));
      rect(64 + (int) ((width-104)*(a.value()-start.value())/totalDuration.value()), 47+n*24, ceil((float) ((width-104)*(duration.value()/totalDuration.value()))), 10);
    }
    
    fill(full);
    stroke(0);
    strokeWeight(1);
    text(nameLookup.get(names[n]), 16, 40+n*24+17);
    
    activeTime.set(names[n], (float) totalTime);
  }
  
  
  //DISPLAY SEASON INDICATORS
  
  for(int i = 0; i < seasonTimes.size(); i++) {
    Time t = seasonTimes.get(i);
    
    stroke(light);
    strokeWeight(1);
    
    float pos = 64 + (float) ((width-104)*(t.value()-start.value())/totalDuration.value());
    
    line(pos, 32, pos, 40+24*26+48);
  }
  
  
  //DISPLAY YEAR INDICATORS
  
  for(int i = 0; i < yearTimes.size(); i++) {
    Time t = yearTimes.get(i);
    
    stroke(lightest);
    strokeWeight(1);
    
    float pos = 64 + (float) ((width-104)*(t.value()-start.value())/totalDuration.value());
    
    line(pos, 40+24*26+8, pos, 40+24*26+24);
  }
  
  line(0, 40+24*26+12, width, 40+24*26+12);
  
  
  //SORTING
  
  totalVideos.sortValuesReverse();
  println(totalVideos);
  println();
  
  activeTime.sortValuesReverse();
  println(activeTime);
  println();
  
  joinDate.sortValues();
  println(joinDate);
  
  saveStrings("data/meta.csv", activeTime.keyArray());
  //run program twice to see new sort
  
  saveFrame("output/output_"+detailLevel+".png");
}

color activityColor(Time duration, String name) {
  if(detailLevel == 3) {
    if(greaterThan(duration, duration(0,6,0))) return color(0, 0, 0);
    if(greaterThan(duration, duration(0,2,0))) return color(47, 0, 0);
    if(greaterThan(duration, duration(0,1,0))) return color(127, 0, 0);
    if(greaterThan(duration, duration(0,0,14))) return color(255, 255, 0);
    if(greaterThan(duration, duration(0,0,7))) return color(0, 255, 0);
    if(greaterThan(duration, duration(0,0,3))) return color(0, 255, 255);
    if(greaterThan(duration, duration(0,0,1))) return color(0, 0, 255);
    return color(255, 0, 255);
  }
  
  int[] col = colorLookup.get(name);
  if(detailLevel == 2) {
    if(greaterThan(duration, duration(0,2,0))) return color(255);
    if(greaterThan(duration, duration(0,0,21))) return color(255-(255-col[0])/2, 255-(255-col[1])/2, 255-(255-col[2])/2);
    return color(col[0], col[1], col[2]);
  } else if(detailLevel == 1) {
    if(greaterThan(duration, duration(0,3,0))) return color(255);
    return color(col[0], col[1], col[2]);
  }
  
  return color(0);
}
