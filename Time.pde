class Time {
  double value;
  
  public Time(double timeValue) {
    value = timeValue;
  }
  
  public Time(int year, int month, int day, int hour, int minute, double second) {
    value = 1000 * (second + 60*(minute + 60*(hour + 24 * dateValue(year, month, day))));
  }
  
  public Time(int year, int month, int day) {
    value = 1000 * 60 * 60 * 24 * (double) dateValue(year, month, day);
  }
  
  int year() {
    int dv = (int) (value/(24*60*60*1000));
    int i = 1970;
    int sum = 0;
    while(sum <= dv) {
      sum += isLeap(i) ? 366 : 365;
      i++;
    }
    return i - 1;
  }
  
  int month() {
    int dv = (int) (value/(24*60*60*1000));
    int i = 1970;
    int sum = 0;
    while(sum <= dv) {
      sum += isLeap(i) ? 366 : 365;
      i++;
    }
    i--;
    sum -= isLeap(i) ? 366 : 365;
    dv -= sum;
    
    int[] monthLengths = {31, isLeap(i) ? 29 : 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31};
    sum = 0;
    i = 0;
    while(sum <= dv) {
      sum += monthLengths[i];
      i++;
    }
    return i;
  }
  
  int day() {
    int dv = (int) (value/(24*60*60*1000));
    int i = 1970;
    int sum = 0;
    while(sum <= dv) {
      sum += isLeap(i) ? 366 : 365;
      i++;
    }
    i--;
    sum -= isLeap(i) ? 366 : 365;
    dv -= sum;
    
    int[] monthLengths = {31, isLeap(i) ? 29 : 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31};
    sum = 0;
    i = 0;
    while(sum <= dv) {
      sum += monthLengths[i];
      i++;
    }
    i--;
    sum -= monthLengths[i];
    dv -= sum;
    
    return dv + 1;
  }
  
  int hour() {
    return (int) (value/(60*60*1000))%24;
  }
  
  int minute() {
    return (int) (value/(60*1000))%60;
  }
  
  double second() {
    return (value/(1000))%60;
  }
  
  double value() {
    return value;
  }
  
  String formatDate() {
    return year() + "-" + month() + "-" + day();
  }
  
}

Time isoTimestamp(String timestamp) {
  int year = parseInt(timestamp.substring(0,4));
  int month = parseInt(timestamp.substring(5,7));
  int day = parseInt(timestamp.substring(8,10));
  int hour = parseInt(timestamp.substring(11,13));
  int minute = parseInt(timestamp.substring(14,16));
  int i = 0;
  for(i = 17; i < timestamp.length(); i++) {
    if(!isNumerical(timestamp.charAt(i))) break;
  }
  double second = parseFloat(timestamp.substring(17,i));
  
  if(timestamp.length() > i+1) {
    int offsetMultiplier = (timestamp.charAt(i)=='+') ? 1 : -1;
    int offsetHours = offsetMultiplier * parseInt(timestamp.substring(i+1,i+3));
    int offsetMinutes = offsetMultiplier * parseInt(timestamp.substring(i+4,i+6));
    
    return new Time(year, month, day, hour - offsetHours, minute - offsetMinutes, second);
  }
  return new Time(year, month, day, hour, minute, second);
}

int dateValue(int year, int month, int day) {
  return yearDays(year) - yearDays(1970) + monthDays(month, isLeap(year)) + day - 1;
}

boolean isLeap(int year) {
  return year%4==0 && year%100!=0 || year%400==0;
}

int yearDays(int year) {
  year--;
  return 365*year + year/4 - year/100 + year/400;
}

int monthDays(int month, boolean isLeap) {
  int[] monthLengths = {31, isLeap ? 29 : 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31};
  int sum = 0;
  for(int i = 0; i < month-1; i++) {
    sum += monthLengths[i];
  }
  return sum;
}

boolean isNumerical(char character) {
  switch(character) {
    case '0': return true;
    case '1': return true;
    case '2': return true;
    case '3': return true;
    case '4': return true;
    case '5': return true;
    case '6': return true;
    case '7': return true;
    case '8': return true;
    case '9': return true;
    case '.': return true;
    default: return false;
  }
}

boolean greaterThan(Time a, Time b) {
  return a.value() > b.value();
}

Time subtract(Time a, Time b) {
  return new Time(a.value() - b.value());
}

Time duration(int years, int months, int days) {
  return new Time(1000 * 60 * 60 * 24 * (double) (days + 30 * (months + 12 * years)));
}
